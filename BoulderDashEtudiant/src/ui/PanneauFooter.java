package ui;


import java.util.HashMap;

import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;

import modele.exceptions.BoulderException;
import objet.Rockford;


public class PanneauFooter extends HBox {
	//�̳�ˮƽ����
	/**
	 * Timer qui se d��clenche tous les 0.1 s
	 */
	private	Timer	timer = new Timer(0.1);
	

	public PanneauFooter(Rockford rockford, Canvas grillePane, HashMap<Class<?>, Image> tabImage) {
		super();
// YL : j'ajoute uniquement l'afficheur du temps ��coul��. Vous pouvez enrichir cette partie de l'interface
// avec le score, le nombre de vies, etc.
		
		// afficheur est un observateur du timer
		AfficheurTempsEcoule afficheur = new AfficheurTempsEcoule(this, rockford);
		try {
			timer.add(afficheur);
			timer.add(new Gravite(rockford.getGrille(), grillePane, tabImage));
		} catch (BoulderException e) {
			e.printStackTrace();
		}
	}

	

}
