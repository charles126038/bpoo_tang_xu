package ui;

import java.util.HashMap;

import application.Grille;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import modele.obs.Observable;
import modele.obs.Observateur;
import objet.Objet;

public class Gravite implements Observateur{

	private Grille grille;
	private Canvas grillePane;
	private HashMap<Class<?>, Image> tabImage;
	
	public Gravite(Grille grille, Canvas grillePane, HashMap<Class<?>, Image> tabImage) {
		super();
		this.grille = grille;
		this.grillePane = grillePane;
		this.tabImage = tabImage;
	}

	@Override
	public void recevoirNotification(Observable observable) { // grille.gravite il y n'aura pas besoins de param
		try {
			grille.gravite();
			dessinerGrille();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	private void dessinerGrille() {
		for (int l = 0; l < grille.getLigne(); l++) {
			for (int c = 0; c < grille.getColonne(); c++) {
				Objet objet = grille.getObjets()[l][c];
				Image image = tabImage.get(objet.getClass());
				getGrillePane().getGraphicsContext2D().drawImage(image, c * 64, l * 64);
			}
		}
	}
	
	public Canvas getGrillePane() {
		return grillePane;
	}

}
