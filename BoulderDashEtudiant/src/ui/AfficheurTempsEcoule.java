package ui;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import modele.obs.Observable;
import modele.obs.Observateur;
import objet.Rockford;

public class AfficheurTempsEcoule implements Observateur {
	private	double	secondes = 0;
	private	Label labelTempsEcoule = new Label("");
	private Rockford rockford;
	
	private final static String LABEL = "Temps écoulé : ";
	private final static String VIE = "         Vie : ";
	private final static String POINT = "   Point : ";

	public AfficheurTempsEcoule(Pane panneau, Rockford rockford) {
		//往布局中添加组件
		//另对象中未定义的rockford 等于输进去的rockford
		panneau.getChildren().add(labelTempsEcoule);
		this.rockford = rockford;
	}

	@Override
	public void recevoirNotification(Observable observable) {
		Timer timer = (Timer)observable;
		secondes += timer.getLaps();
		
		// YL : astuces pour formater un réel avec 1 seul chiffre aprés la virgule
		String strSecondes = String.format("%.1f", secondes);
		//前面不是更新了label 了吗，现在只是往里面放信息更新
		labelTempsEcoule.setText(LABEL+strSecondes+VIE+rockford.getVie()+POINT+rockford.getPoint());
	}
	
	
	
}
