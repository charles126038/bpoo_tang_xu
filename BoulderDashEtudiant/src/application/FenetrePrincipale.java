package application;

import java.util.HashMap;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import modele.exceptions.BoulderMortException;
import objet.*;
import ui.PanneauFooter;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;

public class FenetrePrincipale extends Application {
	// canvas est un outil de dessin
	private Canvas grillePane;
	//BorderPane c'est un conteneur
	private BorderPane root;
	
	private Scene scene;
	//afficher des information en bas de la fenetre
	private PanneauFooter panneauFooter;

	// YL : Les déclarations ci-dessous devront être remplacées par des classes et
	// des
	// objets que vous devez développer
	// <----
	//创建一个图像与类产生连接的哈希表
	private HashMap<Class<?>, Image> tabImage;
	private Grille grille;
	private Rockford rockford;
	 
	// --->
	
	// on definit tous les images
	public static final Image Terre = new Image(FenetrePrincipale.class.getResourceAsStream("/Terre.png"));
	public static final Image Rockford = new Image(FenetrePrincipale.class.getResourceAsStream("/Rockford.png"));
	public static final Image Vide = new Image(FenetrePrincipale.class.getResourceAsStream("/Vide.png"));
	public static final Image Acier = new Image(FenetrePrincipale.class.getResourceAsStream("/Acier.png"));
	public static final Image Rocher = new Image(FenetrePrincipale.class.getResourceAsStream("/Rocher.png"));
	public static final Image Diamant = new Image(FenetrePrincipale.class.getResourceAsStream("/Diamant.png"));
	public static final Image Monstre = new Image(FenetrePrincipale.class.getResourceAsStream("/Monstre.png"));
	public static final Image Brique = new Image(FenetrePrincipale.class.getResourceAsStream("/Brique.png"));
	
	public Canvas getGrillePane() {
		return grillePane;
	}

	public Grille getGrille() {
		return grille;
	}


	@Override
	public void start(Stage primaryStage) {
		try {
			primaryStage.setTitle("Boulder Dash");
			
			root = new BorderPane(grillePane);
			//将布局放入舞台中
			scene = new Scene(root);
			//往舞台中放入键盘输入信息实施更新
			scene.setOnKeyPressed(new HandlerClavier());
			initImages();
			initGrille();
			
			rockford = new Rockford(0,0, grille);
			
			
			 
			 
			
			//////////////////////////////////////////////////////////////////////////////////////////////
			///au-dessous ce sont des tests pour total de neuf cas ,quand on fait le test pour chaque cas,
			///il faut mettre d'abord scene.setOnKeyPressed(new HandlerClavier()); et rockford = new 
			///Rockford(2,3, grille); en commentaire ,si vous voulez tester le premier cas ,relevez deux
			///symboles /* et */ pour le premier cas .si vous voulez tester le deuxieme cas ,faites de 
			///meme.
			///////////////////////////////////////////////////////////////////////////////////////////////
			
			
			
		      /*
			  
			   
			   
			   
			   //tester  le premier cas :Rockford vers la case vide 
			 
			 
			   //Deplacer Rockford(3,3) vers la case contenant du vide  (3,4)
			   // 0 up 1 down 2 right 3 left
		       // resultat la case(3,3) contient du vide ,la case(3,4)contient Rockford 
			    
			    
			    
			    
			    
			    
			    rockford = new Rockford(3,3,grille);
			    grille.ModifierPane(rockford, 3, 3);
			    rockfordDetecteur = new RockfordDetecteur();
			    RockfordStep(2);
	            if (grille.getObjet(rockford.getX(), rockford.getY()-1) instanceof Vide) {
		  
		  
		          System.out.println("la case(3,3) contient du vide ");
		  
	              }
	            else
				  {
					  
					  System.out.println("il y a une erreur ,la case (3,3) ne contient pas de vide ");
					  
				   }
	             if (grille.getObjet(rockford.getX(), rockford.getY()) instanceof Rockford ) {
			  
		  
		         System.out.println("la case(3,4) contient Rockford ");
		  
	               }
	             else
	              {
		  
		         System.out.println("il y a une erreur ,la case (3,4) ne contient pas de Rockford ");
		  
	               }

	             */
	       
			 
			
			
			
			
			
			
			
			
			
			
			
			/* 
			    tester  le deuxieme cas :Rockford vers la case vide 
			 
			    Deplacer Rockford(4,4) vers la case contenant de l'acier  (5,4)
			    0 up 1 down 2 right 3 left
		        resultat la case(4,4) contient rockford ,la case(5,4)contient de l'acier 
			 
			   
			   
			 
			    rockford = new Rockford(4,4,grille);
			    grille.ModifierPane(rockford, 4, 4);
			    rockfordDetecteur = new RockfordDetecteur();
			    RockfordStep(1);
	            if (grille.getObjet(rockford.getX()+1, rockford.getY()) instanceof Acier) {
		  
		  
		         System.out.println("la case(5,4) contient de l'acier ");
		  
	            }
	           else
	            {
		  
		         System.out.println("il y a une erreur ,la case (5,4) ne contient pas de l'acier ");
		  
	            }
               if (grille.getObjet(rockford.getX(), rockford.getY()) instanceof Rockford ) {
		  
		  
		       System.out.println("la case(4,4) contient Rockford ");
		  
	            }
	          else
	            {
		  
		  
		       System.out.println("il y a une erreur ,la case (4,4) ne contient pas de Rockford ");
		  
	             }

              	*/
			
			
			
			
			
			
			
			
			
			     /*			
			
			 
			    // tester  le troisieme cas :Rockford vers la case monstre
			 
			 
			     //Deplacer Rockford(7,7) vers la case contenant un monstre  (7,8)
			     // 0 up 1 down 2 right 3 left
		         //resultat la case(7,8) contient rockford ,la case(7,7)contient du vide 
			 
			   
			 
			 
			      rockford = new Rockford(7,7,grille);
			      grille.ModifierPane(rockford, 7, 7);
			      rockfordDetecteur = new RockfordDetecteur();
			      RockfordStep(2);
	              if (grille.getObjet(rockford.getX(), rockford.getY()-1) instanceof Vide) {
		  
		  
		          System.out.println("la case(7,7) contient du vide ");
		  
	              }
	             else
	              {
		  
		         System.out.println("il y a une erreur ,la case (7,7) ne contient pas de vide ");
		  
	              }
                 if (grille.getObjet(rockford.getX(), rockford.getY()) instanceof Rockford ) {
		  
		  
		         System.out.println("la case(7,8) contient Rockford ");
		         System.out.println(rockford.getVie());
	              }
	             else
	              {
		  
		  
		         System.out.println("il y a une erreur ,la case (7,8) ne contient pas de Rockford ");
		  
	              }

                    
                    
                    */ 	

	            
			
			
			
			
			
			
			
			
			        /*
			
			
			          // tester  le quatrieme cas :Rockford vers la case monstre
	 
	 
	                  //Deplacer Rockford(7,7) vers la case contenant un monstre  (7,8)
	                  // 0 up 1 down 2 right 3 left
                      //resultat la case(7,8) contient rockford ,la case(7,7)contient du vide 
	 
	   
   
	 
                  	   rockford = new Rockford(3,3,grille);
	                   grille.ModifierPane(rockford, 3, 3);
	                   rockfordDetecteur = new RockfordDetecteur();
	                   RockfordStep(3);
                       if (grille.getObjet(rockford.getX(), rockford.getY()+1) instanceof Vide) {
  
  
                       System.out.println("la case(3,3) contient du vide ");
  
                        }
                       else
                        {
  
                       System.out.println("il y a une erreur ,la case (3,3) ne contient pas de vide ");
  
                        }
                       if (grille.getObjet(rockford.getX(), rockford.getY()) instanceof Rockford ) {
  
  
                       System.out.println("la case(3,2) contient Rockford ");
  
                        }
                      else
                        {
  
  
                       System.out.println("il y a une erreur ,la case (3,2) ne contient pas de Rockford ");
  
                        }

			
                       */
			
			
			 
			
			  
			
			  /*
			
			
			
			 // tester  le 5eme cas :Rockford vers la case diamant
			 
			 
			 //Deplacer Rockford(1,6) vers la case contenant un monstre  (1,7)
			 // 0 up 1 down 2 right 3 left
		     //resultat la case(1,7) contient rockford ,la case(1,6)contient du vide,rockford a un diamant
			 //de plus,point +1
			 
			   
		      	
			 
			   rockford = new Rockford(6,1,grille);
			   grille.ModifierPane(rockford, 6, 1);
			   rockfordDetecteur = new RockfordDetecteur();
			   RockfordStep(1);
		       if (grille.getObjet(rockford.getX()-1, rockford.getY()) instanceof Vide) {
		  
		  
		       System.out.println("la case(6,1) contient du vide ");
		  
		        }
		       else
		        {
		  
		       System.out.println("il y a une erreur ,la case (6,1) ne contient pas de vide ");
		  
		        }
		       if (grille.getObjet(rockford.getX(), rockford.getY()) instanceof Rockford ) {
		  
		  
		       System.out.println("la case(7,1) contient Rockford ");
		       System.out.println(rockford.getPoint());
		       }
		       else
		       {
		  
		  
		       System.out.println("il y a une erreur ,la case (7,1) ne contient pas de Rockford ");
		  
		       }

					
				
			
			
	           */		
			
			
			
			
			
			
			   /*
			
			
			    // tester  le 6eme cas :Rockford push Rocher a gauche
			 
			 
			    //Deplacer Rockford(3,6) vers la case contenant un rocher  (3,5)
			    // 0 up 1 down 2 right 3 left
		        //resultat la case(3,5) contient rockford ,la case(3,6)contient du vide,la case(3,4)
			    //contient un rocher
			 
			   
			
		
			   rockford = new Rockford(3,6,grille);
			   grille.ModifierPane(rockford, 3, 6);
			   rockfordDetecteur = new RockfordDetecteur();
			   RockfordStep(3);
		       if (grille.getObjet(rockford.getX(), rockford.getY()+1) instanceof Vide) {
		  
		  
		       System.out.println("la case(3,6) contient du vide ");
		  
		       }
		       else
		       {
		  
		        System.out.println("il y a une erreur ,la case (3,6)  contient pas de vide ");
		  
		       }
		       if (grille.getObjet(rockford.getX(), rockford.getY()) instanceof Rockford ) {
		  
		  
		       System.out.println("la case(3,5) contient Rockford ");
		       System.out.println(rockford.getPoint());
		       }
		      else
		       {
		  
		  
		      System.out.println("il y a une erreur ,la case (3,5) ne contient pas de Rockford ");
		  
		       }

	       	if(grille.getObjet(rockford.getX(), rockford.getY()-1) instanceof Rocher){
			
			   System.out.println("la case (3,4) contient rocher");
			
		       }
		     else
		      {
			
			  System.out.println("il y a une erreur ,la case (3,4) contient pas de rocher ");
			
		      }
		
		      
		      
		      
		      */
			
			
			
			
			
			
			/*
		  
		      tester 7eme cas: pousse le roucher a dorite
		   
		      rockford = new Rockford(3,4,grille);
		      grille.ModifierPane(rockford, 3, 4);
		      rockfordDetecteur = new RockfordDetecteur();
		
		  
		      RockfordStep(2);
	          if (grille.getObjet(rockford.getX(), rockford.getY()-1) instanceof Vide) {
	  
	  
	          System.out.println("la case(3,4) contient du vide ");
	  
	          }
	          else
	          {
	  
	          System.out.println("il y a une erreur ,la case (3,4)  contient pas de vide ");
	  
	          }
	          if (grille.getObjet(rockford.getX(), rockford.getY()) instanceof Rockford ) {
	  
	  
	          System.out.println("la case(3,5) contient Rockford ");
	          System.out.println(rockford.getPoint());
	          }
	         else
	          {
	  
	  
	         System.out.println("il y a une erreur ,la case (3,5) ne contient pas de Rockford ");
	  
	          }

	         if(grille.getObjet(rockford.getX(), rockford.getY()+1) instanceof Rocher){
		
		     System.out.println("la case (3,6) contient rocher");
		
	          }
	         else
	          {
		
		     System.out.println("il y a une erreur ,la case (3,6) contient pas de rocher ");
		
	          }
		
		     
		     
		     
		     */
			
		     /*
			
			
			
			   //  tester 8eme cas :faire tomber le diamant et des rochers
	
		       //faire tomber le diamangt et la goutte de roucher est parfait 
		       //faire tomber les rocher  vers le bas il bien teste dans la fenetre.	
			
			   rockford = new Rockford(6,1,grille);
			   grille.ModifierPane(rockford, 6, 1);
			   rockfordDetecteur = new RockfordDetecteur();
			   RockfordStep(1);
		       if (grille.getObjet(rockford.getX()-1, rockford.getY()) instanceof Vide) {
		  
		  
		       System.out.println("la case(6,1) contient du vide ");
		  
		        }
		       else
		        {
		  
		       System.out.println("il y a une erreur ,la case (6,1)  contient pas de vide ");
		  
		        }
		       if (grille.getObjet(rockford.getX(), rockford.getY()) instanceof Rockford ) {
		  
		  
		       System.out.println("la case(7,1) contient Rockford ");
	           //le vie et le point montre en bas de la fenetre
		        }
		      else
		        {
		  
		  
		      System.out.println("il y a une erreur ,la case (7,1) ne contient pas de Rockford ");
		  
		
		        }

		      
		      
		      
		      */
			
			   
			
			
			   /*
			
			
			   //tester le 9eme cas :faire tomber le rocher jusqu'a ce qu'il rencontre rockford qui
			   //doit perdre une vie 
			
			
			
			   rockford = new Rockford(6,0,grille);
			   grille.ModifierPane(rockford, 6, 0);
			   rockfordDetecteur = new RockfordDetecteur();
			   RockfordStep(1);
		       if (grille.getObjet(rockford.getX()-1, rockford.getY()) instanceof Vide) {
		  
		  
		       System.out.println("la case(6,1) contient du vide ");
		  
		       }
		       else
		       {
		  
		       System.out.println("il y a une erreur ,la case (6,1)  contient pas de vide ");
		  
		       }
		       if (grille.getObjet(rockford.getX(), rockford.getY()) instanceof Rockford ) {
		  
		       System.out.println("la case(7,1) contient Rockford ");
	           //le vie et le point montre en bas de la fenetre
	
		       }
		      else
		       {
		  
		  
		       System.out.println("il y a une erreur ,la case (7,1) ne contient pas de Rockford ");
		  
		
		       }	
			
		       
		       
		       */	
			
			
			
			
			//人物位置更新
			grille.ModifierPane(rockford, rockford.getX(), rockford.getY());
			//更新底部数据信息
			initFooter();
			//把图画一下
			dessinerGrille();
		
		
			
			
			
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// YL : à compléter pour utiliser toutes les images nécessaires
	private void initImages() {
		//更新哈希表
		tabImage = new HashMap<Class<?>, Image>();
		
		tabImage.put(Rockford.class, Rockford);
		
		tabImage.put(Vide.class, Vide);
		
		tabImage.put(Terre.class, Terre);
		
		tabImage.put(Acier.class, Acier);
		
		tabImage.put(Brique.class, Brique);
		
		tabImage.put(Diamant.class, Diamant);
		
		tabImage.put(Monstre.class, Monstre);
		
		tabImage.put(Rocher.class, Rocher);
	}

	private void dessinerGrille() {
		for (int l = 0; l < grille.getLigne(); l++) {
			for (int c = 0; c < grille.getColonne(); c++) {
				Objet objet = grille.getObjets()[l][c];
// YL : On pourrait faire comme ceci si la grille ne contenait pas des entiers
// mais directement la classe de l'objet qui s'y trouve. C'est une solution
// beaucoup plus extensible qu'un entier
				//这里根本没有必要强制类型转换因为你也不知道是哪个类型
				Image image = tabImage.get(objet.getClass());

// YL : Mais pour l'instant, on travaille avec l'entier qui se trouve dans la case
//				Image image = tabImage.get(objet);
//其实这个64，我一直不理解，c，l我可以理解为在指定位置放入图片
				getGrillePane().getGraphicsContext2D().drawImage(image, c * 64, l * 64);
			}
		}
	}

// YL : à remanier complètrement pour tenir compte de vos classes
	private void initGrille() {
		
		// YL : Initialisation en dur. Faudra faire mieux !
		// notamment lire un fichier décrivant le contenu d'un plateau
		
		//grille = new Grille(rockford);
		grille = new Grille(this.getClass().getResource("/grille1.txt").getPath(), rockford);
		
		
		int lGrille = 64 * grille.getLigne();
		int hGrille = 64 * grille.getColonne();
		grillePane = new Canvas(lGrille, hGrille);
		((BorderPane) root).setCenter(grillePane);

		//设置一下获得2D图像
		grillePane.getGraphicsContext2D();

	}

	public static void main(String[] args) {
		launch(args);
	}

	private final class HandlerClavier implements EventHandler<KeyEvent> {
	//	定义了一个键盘实施跟新的量
		public void handle(KeyEvent ke) {
			// YL : il faudra naturellement remanier cette fonction pour qu'elle
			// utilise vos classes...
			try {
				switch (ke.getCode()) {
					case UP: {
						RockfordStep(0);
						break;
					}
					case DOWN: {
						RockfordStep(1);
						break;
					}
					case RIGHT: {
						RockfordStep(2);
						break;
					}
					case LEFT: {
						RockfordStep(3);
						break;
					}
					default:
						return;
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			dessinerGrille();
		}
	}
	

	
	/**
	 * les action de Rockford
	 * @param n  direction du deplacement
	 * @throws BoulderMortException
	 */
	private void RockfordStep(int n) throws Exception {
		Objet objetA = null;
		switch (n) {
		case 0:
				objetA = grille.getObjet(rockford.getX()-1, rockford.getY());
			break;
		case 1:
				objetA = grille.getObjet(rockford.getX()+1, rockford.getY());
			break;
		case 2:
				objetA = grille.getObjet(rockford.getX(), rockford.getY()+1);
			break;
		case 3:
				objetA = grille.getObjet(rockford.getX(), rockford.getY()-1);
			break;
		default:
			break;
		}
		
		grille.RockfordDeplacement(rockford, objetA);

	}
	

	private void initFooter() {
		panneauFooter = new PanneauFooter(rockford, grillePane, tabImage);
		((BorderPane) root).setBottom(panneauFooter);
	}
	
	

}
