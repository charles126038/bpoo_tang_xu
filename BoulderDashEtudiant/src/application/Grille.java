package application;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


import detecteur.Detecteur;
import detecteur.DiamantTompe;
import detecteur.RocherDeplacement;
import detecteur.RocherVersMonstre;
import detecteur.RocherVersRockford;
import detecteur.RockfordVersDiamant;
import detecteur.RockfordVersMonstre;
import detecteur.RockfordVersRocher;
import detecteur.RockfordVersTerre;
import detecteur.RockfordVersVide;
import objet.*;
import remplacant.Remplacant;

public class Grille {
	public static final int NB_LIG = 10;  //nomber de lignes
	public static final int NB_COL = 10;  //nombre de colonnes
	
	//grille 这个类中存储了两个int 量 ，以及类型的表格
	private int ligne;
	private int colonne;
	private Objet[][] objets; // une tableau des objets

	
	private Detecteur detecteur;
	private Rockford rockford;
	
	
//setters et getters
	public int getLigne() {
		return ligne;
	}

	public int getColonne() {
		return colonne;
	}

	public Objet[][] getObjets() {
		return objets;
	}

	public void setObjets(Objet[][] objets) {
		this.objets = objets;
	}
	
	public Rockford getRockford() {
		return rockford;
	}

	public Grille(Rockford rockford) {
		this.rockford = rockford;
		ligne = NB_LIG;
		colonne = NB_COL;
		objets = new Objet[NB_LIG][NB_COL]; 
		for (int l = 0; l < NB_LIG; l++) {
			for (int c = 0; c < NB_COL; c++) {
				objets[l][c] = new Vide(l,c,this);
			}
		}
		
		/*****************************************************
		*******              plateau1
		******************************************************/
		//-------------------------------------->
		//designer les aciers
		objets[5][4] = new Acier(5,4,this); objets[5][5] = new Acier(5,5,this); objets[6][4] = new Acier(6,4,this); objets[6][5] = new Acier(6,5,this);
		
		//designer les terres
		for (int c = 0; c < NB_COL; c++) {
			objets[0][c] = new Terre(0,c,this);
		}
		for (int c = 0; c < NB_COL; c++) {
			if(c != 1 && c != 5)
				objets[1][c] = new Terre(1,c,this);
		}
		for (int c = 0; c < NB_COL; c++) {
			if(c != 1 && c != 2 && c != 3)
				objets[2][c] = new Terre(2,c,this);
		}
		for (int c = 0; c < NB_COL; c++) {
			if(c == 1 || c == 2 || c == 8)
				objets[3][c] = new Terre(3,c,this);
		}
		for (int c = 0; c < NB_COL; c++) {
			objets[4][c] = new Terre(4,c,this);
		}
		for (int c = 0; c < NB_COL; c++) {
			if(c != 4 && c != 5 &&c!=1 && c!=0 )
				objets[6][c] = new Terre(6,c,this);
		}
		for (int c = 0; c < NB_COL; c++) {
			if(c != 3 && c != 4 && c != 5)
				objets[8][c] = new Terre(8,c,this);
		}
		objets[7][6] = new Terre(7,6,this);
		
		//designer les roches
		objets[3][5] = new Rocher(3,5,this); objets[2][1] = new Rocher(2,1,this); objets[2][2] = new Rocher(2,2,this);
		objets[5][0] = new Rocher(5,0,this);  objets[5][2] = new Rocher(5,2,this); objets[5][3] = new Rocher(5,3,this);
		
		for (int c = 0; c < NB_COL; c++) {
			if(c != 3 && c != 4 && c != 5)
				objets[9][c] = new Rocher(9,c,this);
		}
		
		//designer la diamant
		objets[5][1] = new Diamant(5,1,this);
		
		//designer le monstre
		objets[7][8] = new Monstre(7,8,this);
		//<--------------------------------------------
		
		ConstruitChaineRespon();
	}
	
	/**
	 * initialiser le grille par fichier
	 * @param fichier
	 */
	public Grille(String fichier, Rockford rockford) {
		this.rockford = rockford;
		BufferedReader lecteurAvecBuffer = null;
	    String line;
	    try
	    {
	    	//mettre le txt
	    	lecteurAvecBuffer = new BufferedReader(new FileReader(fichier));
	    	ligne = 0;
	    	colonne = 0;
			while(true) {  // obtenir le nombre de lignes
				
				line = lecteurAvecBuffer.readLine();
				
				if(line != null) {
				
					ligne = Integer.parseInt(line);
					break;
				}
				else
					break;
			}
			while(true) {  // obtenir le nombre de colonnes
				line = lecteurAvecBuffer.readLine();
				if(line != null) {
					colonne = Integer.parseInt(line);
					break;
				}
				else
					break;
			}
			objets = new Objet[ligne][colonne];
			String[] content;
			for(int i = 0; i < ligne; i++) {
				
				line = lecteurAvecBuffer.readLine();
				//par exemple line =A B C D
				//Content[0]=A Content[1]=B Content[2]=C Content[3]=D
				content = line.split(" ");
				
				for(int j = 0; j < colonne; j++) {
					objets[i][j] = selecteur(content[j], i, j);
				}
			}
			lecteurAvecBuffer.close();
	    }
	    catch(FileNotFoundException exc) {
	    	System.out.println("Erreur d'ouverture du fichier");
	    } catch (IOException e) {
			e.printStackTrace();
		}
	    
	    ConstruitChaineRespon();
	}
	
	/**
	 * Construire une chaine de responsabilitee
	 */
	public void ConstruitChaineRespon() {
		detecteur = new RockfordVersVide();
		RockfordVersTerre rockfordVersTerre= new RockfordVersTerre();
		RockfordVersDiamant rockfordVersDiamant = new RockfordVersDiamant();
		RockfordVersMonstre rockfordVersMonstre = new RockfordVersMonstre();
		RockfordVersRocher rockfordVersRocher = new RockfordVersRocher();
		RocherDeplacement rocherDeplacement = new RocherDeplacement();
		RocherVersMonstre rocherVersMonstre = new RocherVersMonstre();
		RocherVersRockford rocherVersRockford = new RocherVersRockford();
		DiamantTompe diamantTompe = new DiamantTompe();
		
		ajouterDeterteur(rockfordVersRocher);
		ajouterDeterteur(rockfordVersMonstre);
		ajouterDeterteur(rockfordVersDiamant);
		ajouterDeterteur(rockfordVersTerre);
		ajouterDeterteur(diamantTompe);
		ajouterDeterteur(rocherDeplacement);
		ajouterDeterteur(rocherVersMonstre);
		ajouterDeterteur(rocherVersRockford);
	}
	
	/**
	 * Modifier la grille
	 * @param obj
	 * @param x
	 * @param y
	 */
	//modifier un objet dans la position (ligne,colonne)
	public void ModifierPane(Objet obj, int ligne, int colonne) {
		objets[ligne][colonne] = obj;
	}
	
	/**
	 * detecter tous les objets dans la grille pour les appliquer gravite
	 */
	public void gravite() throws Exception {
		for(int i = 0; i < ligne-1; i++) {
			for(int j = 0; j < colonne; j++) {
				if(objets[i][j] instanceof ObjetGravite) {
					assert detecteur != null : "Erreur : la chaine de responsabilité est vide...";
					Remplacant tomber = detecteur.detecter(objets[i][j], objets[i+1][j], this);
					if (tomber != null)
						tomber.traiter();
				}
			}
		}
	}
	
	/**
	 * detecter le deplacement du rockford
	 * @param objetA
	 * @throws Exception
	 */
	public void RockfordDeplacement(Objet objetD, Objet objetA) throws Exception {
		assert detecteur != null : "Erreur : la chaine de responsabilité est vide...";
		if(objetA != null) {
			Remplacant remplacant = detecteur.detecter(objetD, objetA, this);
			if (remplacant != null)
				remplacant.traiter();
		}
	}
	
	/**
	 * Obtenir l'objet
	 * @param x
	 * @param y
	 * @return
	 */
	//get la classe 
	public Objet getObjet(int x, int y) {
		//pour verifier la position is not out of bounds
		if(x<0 || x>ligne-1 || y<0 || y>colonne-1)  // out of bounds
			return null;
		return objets[x][y];
	}
	
	/**
	 * V vide, T terre, S rockford, R rocher, A acier, B brique, D diamant, M monstre
	 * @param s
	 * @param x
	 * @param y
	 * @return
	 */
	
	//choisir des informations dans le grille1.txt 
	private Objet selecteur(String s, int x, int y) {
		switch (s) {
		case "V":
			return new Vide(x, y,this);
		case "T":
			return new Terre(x, y,this);
		case "S":
			return new Rockford(x, y,this);
		case "R":
			return new Rocher(x, y,this);
		case "A":
			return new Acier(x, y,this);
		case "B":
			return new Brique(x, y,this);
		case "D":
			return new Diamant(x, y,this);
		case "M":
			return new Monstre(x, y,this);

		default:
			return null;
		}
	}
	
	/**
	 * ajouter les detecteurs
	 * @param d
	 */
	private void ajouterDeterteur(Detecteur d) {
		assert d != null : "Le detecteur ne doit pas être null";
		// Insertion du nouveau détecteur en tête de liste
		d.setSuivant(detecteur);
		detecteur = d;
	}

	
}
