package objet;

import application.Grille;

public abstract class Objet {
	//Pour tous les Objets 
	//int x,int y,Grille grille sont communs
	protected int x;  //ordonnee x
	protected int y;  //ordonnee y
	protected Grille grille;
//dans la classe abstraite ,on peut definir des methodes abstraites et des methodes pas de abstraites
	public Objet(int x, int y, Grille grille) {
		super();
		this.x = x;
		this.y = y;
		this.grille = grille;
	}
//getters et setters
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

}
