package objet;

import application.Grille;
import modele.exceptions.BoulderMortException;

public class Rockford extends Objet{
	
	public Rockford(int x, int y, Grille grille) {
		super(x, y, grille);
		this.vie = 3;
		this.point = 0;
	}

	private int vie;
	private int point;

	
	public void loseLife() throws BoulderMortException { // perdre vie
		this.vie--;
		if(this.getVie() <= 0) { // mort exception
			throw new BoulderMortException("Rockford est mort");
		}
	}
	
	public void addPoint() { // gagner point
		this.point++;
	}

	public int getVie() {
		return vie;
	}

	public int getPoint() {
		return point;
	}

	public Grille getGrille() {
		return grille;
	}
	
	
}
