package detecteur;

import application.Grille;
import modele.exceptions.BoulderMortException;
import objet.Objet;
import objet.Rocher;
import objet.Vide;
import remplacant.Remplacant;
import remplacant.ObjetDeplacement;

public class RocherDeplacement extends Detecteur{
	private ObjetDeplacement rocherDeplacement;

	@Override
	public boolean estDetectee(Objet objetD, Objet objetA, Grille grille) throws BoulderMortException {
		rocherDeplacement = new ObjetDeplacement(objetD, objetA, grille);
		Objet barreLeft;
		Objet barreRight;
		Objet barreLD;
		Objet barreRD;

		barreLeft = grille.getObjet(objetD.getX(), objetD.getY()-1);  // obtenir l'objet a gauche du objetD
		
		barreRight = grille.getObjet(objetD.getX(), objetD.getY()+1);  // obtenir l'objet a droite du objetD

		barreLD = grille.getObjet(objetA.getX(), objetA.getY()-1);  // obtenir l'objet a gauche du rocherDown

		barreRD = grille.getObjet(objetA.getX(), objetA.getY()+1);  // obtenir l'objet a droite du rocherDown
		
		if(objetD instanceof Rocher && objetA instanceof Vide) {  // tomber
			return true;
		}
		
		if(objetD instanceof Rocher && objetA instanceof Rocher) {  // rouler
			if(barreRight != null && barreRight instanceof Vide) {
				if(barreRD != null && barreRD instanceof Vide) {  // rouler a droite
					rocherDeplacement = new ObjetDeplacement(objetD, barreRight, grille);
					return true;
				}
			}
		
			if(barreLeft != null && barreLeft instanceof Vide) {
				if(barreLD != null && barreLD instanceof Vide) {  // rouler a gauche
					rocherDeplacement = new ObjetDeplacement(objetD, barreLeft, grille);
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public Remplacant extraire() {
		return rocherDeplacement;
	}

}
