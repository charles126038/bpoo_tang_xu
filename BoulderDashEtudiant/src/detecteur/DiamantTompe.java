package detecteur;

import application.Grille;
import modele.exceptions.BoulderMortException;
import objet.Diamant;
import objet.Objet;
import objet.Rockford;
import objet.Vide;
import remplacant.ObjetDeplacement;
import remplacant.Remplacant;

public class DiamantTompe extends Detecteur{
	private ObjetDeplacement tomber;

	@Override
	public boolean estDetectee(Objet objetD, Objet objetA, Grille grille) throws BoulderMortException {
		tomber = new ObjetDeplacement(objetD, objetA, grille);
		
		if(objetD instanceof Diamant && objetA instanceof Vide) {
			if(grille.getObjet(objetA.getX()+1, objetA.getY()) != null) {
				if(grille.getObjet(objetA.getX()+1, objetA.getY()) instanceof Rockford) { // si rockford est en-dessous de l'objet
					tomber = new ObjetDeplacement(objetD, null, grille);
					Rockford rockford = (Rockford)grille.getObjet(objetA.getX()+1, objetA.getY());
					rockford.loseLife();
					rockford.addPoint();
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public Remplacant extraire() {
		return tomber;
	}

}
