package detecteur;

import application.Grille;
import modele.exceptions.BoulderMortException;
import objet.Objet;
import remplacant.Remplacant;

public abstract class Detecteur {
	private Detecteur suivant;
	
	/**
	 * 
	 * @param objetD  objet depart
	 * @param objetA  objet arriver
	 * @param grille
	 * @return
	 * @throws BoulderMortException
	 */
	public abstract boolean estDetectee(Objet objetD, Objet objetA, Grille grille) throws BoulderMortException;
	
	public abstract Remplacant extraire();

	public Detecteur getSuivant() {
		return suivant;
	}

	public void setSuivant(Detecteur suivant) {
		this.suivant = suivant;
	}
	
	public Remplacant detecter(Objet objetD, Objet objetA, Grille grille) throws BoulderMortException {
		if(estDetectee(objetD, objetA, grille))
			return extraire();
		else if(suivant != null)
			return suivant.detecter(objetD, objetA, grille);
		else
			return null;
	}
}
