package detecteur;

import application.Grille;
import modele.exceptions.BoulderMortException;
import objet.Monstre;
import objet.Objet;
import objet.Rockford;
import remplacant.Remplacant;
import remplacant.RockfordDeplace;

public class RockfordVersMonstre extends Detecteur{
	private RockfordDeplace rockfordAction;
	@Override
	public boolean estDetectee(Objet objetD, Objet objetA, Grille grille) throws BoulderMortException {
		rockfordAction = new RockfordDeplace(objetD, objetA, grille);
		if(objetD instanceof Rockford && objetA instanceof Monstre) {
			Rockford rockford = (Rockford)objetD;
			rockford.loseLife();
			return true;
		}
		return false;
	}

	@Override
	public Remplacant extraire() {
		return rockfordAction;
	}

}
