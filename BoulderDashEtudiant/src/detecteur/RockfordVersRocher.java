package detecteur;

import application.Grille;
import modele.exceptions.BoulderMortException;
import objet.Objet;
import objet.Rocher;
import objet.Rockford;
import objet.Vide;
import remplacant.Remplacant;
import remplacant.RockfordAction;

public class RockfordVersRocher extends Detecteur{
	private RockfordAction rockfordAction;
	@Override
	public boolean estDetectee(Objet objetD, Objet objetA, Grille grille) throws BoulderMortException {
		rockfordAction = new RockfordAction(objetD, objetA, grille);
		if(objetD instanceof Rockford && objetA instanceof Rocher) {
			if(objetD.getY() < objetA.getY()) {  // rocher est au droite de rockford
				if(grille.getObjet(objetA.getX(), objetA.getY() + 1) != null && grille.getObjet(objetA.getX(), objetA.getY() + 1) instanceof Vide) {
					rockfordAction.setDirection(0);
					return true;
				}
				else
					return false;
			}
			else if(objetD.getY() > objetA.getY()){  // rocher est au gauche de rockford
				if(grille.getObjet(objetA.getX(), objetA.getY() - 1) != null && grille.getObjet(objetA.getX(), objetA.getY() - 1) instanceof Vide) {
					rockfordAction.setDirection(1);
					return true;
				}
				else
					return false;
			}
		}
		return false;
	}

	@Override
	public Remplacant extraire() {
		return rockfordAction;
	}

}
