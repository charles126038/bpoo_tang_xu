package detecteur;

import application.Grille;
import modele.exceptions.BoulderMortException;
import objet.Objet;
import objet.Rockford;
import objet.Terre;
import remplacant.Remplacant;
import remplacant.RockfordDeplace;

public class RockfordVersTerre extends Detecteur{
	private RockfordDeplace rockfordAction;
	@Override
	public boolean estDetectee(Objet objetD, Objet objetA, Grille grille) throws BoulderMortException {
		rockfordAction = new RockfordDeplace(objetD, objetA, grille);
		if(objetD instanceof Rockford && objetA instanceof Terre)
			return true;
		return false;
	}

	@Override
	public Remplacant extraire() {
		return rockfordAction;
	}

}
