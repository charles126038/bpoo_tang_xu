package detecteur;

import application.Grille;
import modele.exceptions.BoulderMortException;
import objet.Objet;
import objet.Rocher;
import objet.Rockford;
import objet.Vide;
import remplacant.ObjetDeplacement;
import remplacant.Remplacant;

public class RocherVersRockford extends Detecteur{
	private ObjetDeplacement rocherDeplacement;
	
	@Override
	public boolean estDetectee(Objet objetD, Objet objetA, Grille grille) throws BoulderMortException {
		rocherDeplacement = new ObjetDeplacement(objetD, objetA, grille);
		if(objetD instanceof Rocher && objetA instanceof Vide) {
			if(grille.getObjet(objetA.getX()+1, objetA.getY()) != null) {
				if(grille.getObjet(objetA.getX()+1, objetA.getY()) instanceof Rockford) { // si rockford est en-dessous de l'objet
					rocherDeplacement = new ObjetDeplacement(objetD, null, grille);
					Rockford rockford = (Rockford)grille.getObjet(objetA.getX()+1, objetA.getY());
					rockford.loseLife();
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public Remplacant extraire() {
		return rocherDeplacement;
	}
	

}
