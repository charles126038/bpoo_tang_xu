package detecteur;

import application.Grille;
import modele.exceptions.BoulderMortException;
import objet.Diamant;
import objet.Objet;
import objet.Rockford;
import remplacant.Remplacant;
import remplacant.RockfordDeplace;

public class RockfordVersDiamant extends Detecteur{
	private RockfordDeplace rockfordAction;
	@Override
	public boolean estDetectee(Objet objetD, Objet objetA, Grille grille) throws BoulderMortException {
		rockfordAction = new RockfordDeplace(objetD, objetA, grille);
		if(objetD instanceof Rockford && objetA instanceof Diamant) {
			Rockford rockford = (Rockford)objetD;
			rockford.addPoint();
			return true;
		}
		return false;
	}

	@Override
	public Remplacant extraire() {
		return rockfordAction;
	}

}
