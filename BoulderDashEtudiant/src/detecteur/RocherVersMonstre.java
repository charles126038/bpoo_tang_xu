package detecteur;

import application.Grille;
import modele.exceptions.BoulderMortException;
import objet.Diamant;
import objet.Monstre;
import objet.Objet;
import objet.Rocher;
import objet.Vide;
import remplacant.ObjetDeplacement;
import remplacant.Remplacant;

public class RocherVersMonstre extends Detecteur{

	private ObjetDeplacement rocherDeplacement;
	
	@Override
	public boolean estDetectee(Objet objetD, Objet objetA, Grille grille) throws BoulderMortException {
		rocherDeplacement = new ObjetDeplacement(objetD, objetA, grille);
		if(objetD instanceof Rocher && objetA instanceof Vide) {
			if(grille.getObjet(objetA.getX()+1, objetA.getY()) != null) {
				if(grille.getObjet(objetA.getX()+1, objetA.getY()) instanceof Monstre) { // si monstre est en-dessous de l'objet
					Diamant diamant = new Diamant(objetA.getX()+1, objetA.getY(), grille);
					grille.ModifierPane(diamant, objetA.getX()+1, objetA.getY());
				}
			}
		}
		return false;
	}

	@Override
	public Remplacant extraire() {
		return rocherDeplacement;
	}

}
