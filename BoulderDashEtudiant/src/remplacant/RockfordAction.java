package remplacant;

import application.Grille;
import objet.Objet;
import objet.Vide;

public class RockfordAction extends Remplacant{
	private int direction; // la direction rockford deplace l'objet (0 vers droite, 1 vers gauche)

	public RockfordAction(Objet objetD, Objet objetA, Grille grille) {
		super(objetD, objetA, grille);
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	@Override
	public void traiter() throws Exception {
		
		// rockford deplace
		Vide vide = new Vide(objetD.getX(), objetD.getY(), grille);
		grille.ModifierPane(vide, objetD.getX(), objetD.getY());
		objetD.setX(objetA.getX());  // objetD deplace
		objetD.setY(objetA.getY());
		grille.ModifierPane(objetD, objetA.getX(), objetA.getY());
		
		// l'objet deplace
		if(direction == 1) {
			objetA.setY(objetA.getY() - 1);
			grille.ModifierPane(objetA, objetA.getX(), objetA.getY());
		}
		else {
			objetA.setY(objetA.getY() + 1);
			grille.ModifierPane(objetA, objetA.getX(), objetA.getY());
		}
		
	}

	
}
