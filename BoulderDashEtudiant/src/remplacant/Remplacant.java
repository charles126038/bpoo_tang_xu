package remplacant;

import application.Grille;
import objet.Objet;

public abstract class Remplacant {
	protected Objet objetD;
	protected Objet objetA;
	protected Grille grille;
	
	public Remplacant(Objet objetD, Objet objetA, Grille grille) {
		super();
		this.objetD = objetD;
		this.objetA = objetA;
		this.grille = grille;
	}


	public Grille getGrille() {
		return grille;
	}


	public abstract void traiter() throws Exception;
}
