package remplacant;

import application.Grille;
import objet.Objet;
import objet.Vide;

/**
 * Les actions pour objetD
 * modifier la grille
 * @author Administraitro
 *
 */
public class RockfordDeplace extends Remplacant{
	
	public RockfordDeplace(Objet objetD, Objet objetA, Grille grille) {
		super(objetD, objetA, grille);
	}



	@Override
	public void traiter() throws Exception {
		Vide vide = new Vide(objetD.getX(), objetD.getY(), grille);
		grille.ModifierPane(vide, objetD.getX(), objetD.getY());
		objetD.setX(objetA.getX());  // objetD deplace
		objetD.setY(objetA.getY());
		grille.ModifierPane(objetD, objetA.getX(), objetA.getY());
	}

}
