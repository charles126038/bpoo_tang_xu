package remplacant;

import application.Grille;
import objet.Objet;
import objet.Vide;

public class ObjetDeplacement extends Remplacant{
	
	public ObjetDeplacement(Objet objetD, Objet objetA, Grille grille) {
		super(objetD, objetA, grille);
	}


	@Override
	public void traiter() throws Exception {
		
		Vide vide = new Vide(objetD.getX(), objetD.getY(), grille);
		grille.ModifierPane(vide, objetD.getX(), objetD.getY());  // remplace la position origine du rocher par vide
		
		if(objetA != null) {
			objetD.setX(objetA.getX());
			objetD.setY(objetA.getY());
			grille.ModifierPane(objetD, objetA.getX(), objetA.getY());
		}
	}

}
